# Generated by Django 3.1.7 on 2021-03-30 10:02

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Matkul',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_matkul', models.CharField(max_length=30)),
                ('dosen', models.CharField(max_length=50)),
                ('jumlah_sks', models.IntegerField()),
                ('deskripsi', models.CharField(max_length=100)),
                ('semester_tahun', models.CharField(max_length=30)),
                ('ruang_kelas', models.CharField(max_length=50)),
            ],
        ),
    ]
